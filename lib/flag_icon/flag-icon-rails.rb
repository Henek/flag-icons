module FlagIcon
  module Rails
    class << self
      def load!
        if rails?
          register_rails_engine
        elsif sprockets?
          register_sprockets
        end

        configure_sass
      end

      def gem_path
        @gem_path ||= File.expand_path('..', File.dirname(__FILE__))
      end

      def stylesheets_path
        File.join(assets_path, 'stylesheets')
      end

      def icons_path
        File.join(assets_path, 'icons')
      end

      def assets_path
        @assets_path ||= File.join(gem_path, 'assets')
      end

      def sprockets?
        defined?(::Sprockets)
      end

      def rails?
        defined?(::Rails)
      end

      private

      def configure_sass
        require 'sass'

        ::Sass.load_paths << stylesheets_path
      end

      def register_rails_engine
        require 'sass-rails'
        require 'flag_icon/rails/engine'
        require 'flag_icon/rails/railtie'
      end

      def register_sprockets
        Sprockets.append_path(stylesheets_path)
        Sprockets.append_path(icons_path)
      end
    end
  end
end

FlagIcon::Rails.load!