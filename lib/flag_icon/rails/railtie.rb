require "flag-icon/rails/helpers"

module FlagIcon
  module Rails
    class Railtie < ::Rails::Railtie
      initializer "flag-icon-rails.view_helpers" do
        ActionView::Base.send :include, ViewHelpers
      end
    end
  end
end