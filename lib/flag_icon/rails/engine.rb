module FlagIcon
  module Rails
    class Engine < ::Rails::Engine
      initializer 'flag-icon-rails.assets.precompile', group: :all do |app|
        %w(stylesheets icons).each do |sub|
          app.config.assets.paths << root.join('assets', sub).to_s
        end
      end
    end
  end
end